package com.epam.training;

import com.epam.training.beans.Employee;
import com.epam.training.beans.Position;
import com.epam.training.beans.Salary;
import com.epam.training.services.EmployeeService;
import com.epam.training.services.PositionService;
import com.epam.training.services.SalaryService;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Random;

public class Runner {

    private static Logger LOGGER = Logger.getLogger("LOGGER");
    private final static int yearsNumber = 4;
    private final static int monthNumber = 12;

    public static void main(String[] args) {

        ClassPathXmlApplicationContext context
                = new ClassPathXmlApplicationContext("ApplicationContext.xml");
        PositionService positionService = new PositionService();
        EmployeeService employeeService = new EmployeeService();
        SalaryService salaryService = new SalaryService();

        Position juniorPosition = context.getBean("positionJunior", Position.class);
        Position middlePosition = context.getBean("positionMiddle", Position.class);
        Position seniorPosition = context.getBean("positionSenior", Position.class);

        Employee ivanovEmployee = context.getBean("employeeOne", Employee.class);
        Employee petrovEmployee = context.getBean("employeeTwo", Employee.class);
        Employee sidorovEmployee = context.getBean("employeeThree", Employee.class);

        Salary juniorSalary = context.getBean("juniorSalary", Salary.class);
        Salary middleSalary = context.getBean("middleSalary", Salary.class);
        Salary seniorSalary = context.getBean("seniorSalary", Salary.class);

        positionService.createPosition(juniorPosition.getPosition_name(), juniorPosition.getPosition_rate());
        positionService.createPosition(middlePosition.getPosition_name(), middlePosition.getPosition_rate());
        positionService.createPosition(seniorPosition.getPosition_name(), seniorPosition.getPosition_rate());

        employeeService.hireEmployee(ivanovEmployee.getEmployee_name(), ivanovEmployee.getPosition_id());
        employeeService.hireEmployee(petrovEmployee.getEmployee_name(), petrovEmployee.getPosition_id());
        employeeService.hireEmployee(sidorovEmployee.getEmployee_name(), sidorovEmployee.getPosition_id());

        Random random = new Random();

        for (int i = 1; i < yearsNumber; i++) {
            LOGGER.info("Company life, year number " + i + "\n");
            int randomNumber = random.nextInt(5);
            int randomChoose = random.nextInt(2);

            if (randomChoose == 0) {
                String positionName = juniorPosition.getPosition_name() + randomNumber;
                positionService.createPosition(positionName, juniorPosition.getPosition_rate());
                LOGGER.info("Company made new position: " + positionName + "\n");
                positionService.updatePosition(juniorPosition.getPosition_id(),
                        juniorPosition.getPosition_name(), juniorPosition.getPosition_rate());
                LOGGER.info("Company updated position: " + juniorPosition.getPosition_name() + "\n");
                String employeeName = petrovEmployee.getEmployee_name() + randomNumber;
                employeeService.hireEmployee(employeeName, petrovEmployee.getPosition_id());
                LOGGER.info("Company hire new employee: " + employeeName + "\n");
            } else {
                String positionNameToDelete = juniorPosition.getPosition_name();
                positionService.deletePosition(juniorPosition.getPosition_id());
                LOGGER.info("Company deleted position: " + positionNameToDelete + "\n");
                String positionName = juniorPosition.getPosition_name();
                positionService.createPosition(positionName, juniorPosition.getPosition_rate());
                LOGGER.info("Company made new position: " + positionName + "\n");
                String employeeNameToFire = petrovEmployee.getEmployee_name();
                employeeService.fireEmployee(petrovEmployee.getPosition_id());
                LOGGER.info("Company fire the employee: " + employeeNameToFire + "\n");
                String employeeName = petrovEmployee.getEmployee_name();
                employeeService.hireEmployee(employeeName, petrovEmployee.getPosition_id());
                LOGGER.info("Company hire new employee: " + employeeName + "\n");
            }
            for (int j = 0; j < monthNumber; j++) {
                LOGGER.info("Every month salary of each position:\n");
                LOGGER.info("Junior salary: " + (juniorSalary.getSalary() / 100) + "."
                        + (juniorSalary.getSalary() % 100) + " rubls");
                LOGGER.info("Middle salary: " + (middleSalary.getSalary() / 100) + "."
                        + (middleSalary.getSalary() % 100) + " rubls");
                LOGGER.info("Senior salary: " + (seniorSalary.getSalary() / 100) + "."
                        + (seniorSalary.getSalary() % 100) + " rubls");
            }
//            Calculate every year inflation
            juniorSalary.setSalary(salaryService.calculateSalaryWithInflationCoefficient(juniorSalary.getSalary(),
                    juniorSalary.getInflationCoefficient()));
            middleSalary.setSalary(salaryService.calculateSalaryWithInflationCoefficient(middleSalary.getSalary(),
                    middleSalary.getInflationCoefficient()));
            seniorSalary.setSalary(salaryService.calculateSalaryWithInflationCoefficient(seniorSalary.getSalary(),
                    seniorSalary.getInflationCoefficient()));
            LOGGER.info("\n");
        }
        context.close();
    }
}
