package com.epam.training.services;

import com.epam.training.connections.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PositionService {

    private static Logger LOGGER = Logger.getLogger("LOGGER");

    private final static String GET_POSITION = "SELECT * FROM position WHERE position_id = ?";
    private final static String UPDATE_POSITION = "UPDATE position SET position_name = ?, position_rate = ? WHERE position_id = ?";
    private final static String CREATE_POSITION = "INSERT INTO position (position_name, position_rate) VALUES (?, ?)";
    private final static String DELETE_POSITION = "DELETE FROM position WHERE (position_id = ?)";

    /**
     * Method create new position in organisation DB.
     * @param newPosition - position name
     * @param positionRate - new position rate
     */
    public void createPosition(String newPosition, int positionRate) {
        try (Connection cn = ConnectionPool.getConnection();
             PreparedStatement ps = cn.prepareStatement(CREATE_POSITION)) {
            ps.setString(1, newPosition);
            ps.setInt(2, positionRate);
            ps.execute();
        } catch (SQLException e) {
            LOGGER.error(e.toString(), e);
        }
    }

    /**
     * Method display all position fields by id.
     * @param positionId - position id
     */
    public void readPosition(int positionId) {
        try (Connection cn = ConnectionPool.getConnection();
             PreparedStatement ps = cn.prepareStatement(GET_POSITION)) {
            ps.setInt(1, positionId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int position_id = rs.getInt(1);
                String positionName = rs.getString(2);
                int positionRate = rs.getInt(3);
                LOGGER.info("Position id: " + position_id
                        + ",\n position name: " + positionName
                        + ",\n position rate " + positionRate
                        + "\n"
                );
            }
        } catch (SQLException e) {
            LOGGER.error(e.toString(), e);
        }
    }

    /**
     * Method update all position fields by id.
     * @param positionId - position id
     * @param newPosition - new position name
     * @param positionRate - new position rate
     */
    public void updatePosition(int positionId, String newPosition, int positionRate) {
        try (Connection cn = ConnectionPool.getConnection();
             PreparedStatement ps = cn.prepareStatement(UPDATE_POSITION)) {
            ps.setString(1, newPosition);
            ps.setInt(2, positionRate);
            ps.setInt(3, positionId);
            ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.toString(), e);
        }
    }

    /**
     * Method remove position by id.
     * @param positionId - position id
     */
    public void deletePosition(int positionId) {
        try (Connection conn = ConnectionPool.getConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE_POSITION)) {
            ps.setInt(1, positionId);
            ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.toString(), e);
        }
    }
}