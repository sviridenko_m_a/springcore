package com.epam.training.services;

public class SalaryService {

    public SalaryService() {
    }

    /**
     * Method calculate salary considering inflation.
     */
    public int calculateSalaryWithInflationCoefficient(int salary, int coefficient) {
        return salary + coefficient;
    }
}