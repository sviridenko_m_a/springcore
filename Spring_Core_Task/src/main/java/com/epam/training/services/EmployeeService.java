package com.epam.training.services;

import com.epam.training.connections.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class EmployeeService {

    private static Logger LOGGER = Logger.getLogger("LOGGER");
    private final static String CREATE_EMPLOYEE = "INSERT INTO employee (employee_name, position_id) VALUES (?, ?)";
    private final static String DELETE_EMPLOYEE = "DELETE FROM employee WHERE (employee_id = ?)";

    /**
     * Method to hire employee to organisation.
     *
     * @param employeeName - employee name
     * @param positionId   - employee position
     */
    public void hireEmployee(String employeeName, int positionId) {
        try (Connection cn = ConnectionPool.getConnection();
             PreparedStatement ps = cn.prepareStatement(CREATE_EMPLOYEE)) {
            ps.setString(1, employeeName);
            ps.setInt(2, positionId);
            ps.execute();
        } catch (SQLException e) {
            LOGGER.error(e.toString(), e);
        }
    }

    /**
     * Method to fire employee from organisation.
     *
     * @param employeeId - employee id
     */
    public void fireEmployee(int employeeId) {
        try (Connection conn = ConnectionPool.getConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE_EMPLOYEE)) {
            ps.setInt(1, employeeId);
            ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.toString(), e);
        }
    }

}
