package com.epam.training.connections;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class ConnectionPool {

    private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);
    private static HikariConfig config = new HikariConfig("/context.properties");
    private static HikariDataSource hikariDataSource;

    static {
        hikariDataSource = new HikariDataSource(config);
    }

    private ConnectionPool() {
    }

    /**
     * Connection to DB
     *
     * @return connection
     * @throws SQLException SQL exception
     */
    public static Connection getConnection() throws SQLException {
        LOGGER.info("getConnection");
        return hikariDataSource.getConnection();
    }
}