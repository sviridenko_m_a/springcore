package com.epam.training.beans;

public class Position {

    private int position_id;
    private String position_name;
    private int position_rate;

    public Position() {
    }

    public Position(String position_name, int position_rate) {
        this.position_name = position_name;
        this.position_rate = position_rate;
    }

    public Position(int position_id, String position_name, int position_rate) {
        this.position_id = position_id;
        this.position_name = position_name;
        this.position_rate = position_rate;
    }

    public int getPosition_id() {
        return position_id;
    }

    public void setPosition_id(int position_id) {
        this.position_id = position_id;
    }

    public String getPosition_name() {
        return position_name;
    }

    public void setPosition_name(String position_name) {
        this.position_name = position_name;
    }

    public int getPosition_rate() {
        return position_rate;
    }

    public void setPosition_rate(int position_rate) {
        this.position_rate = position_rate;
    }

    @Override
    public String toString() {
        return "Position{" +
                "position_id=" + position_id +
                ", position_name='" + position_name + '\'' +
                ", position_rate=" + position_rate +
                '}';
    }
}