package com.epam.training.beans;

public class Employee {

    private int employee_id;
    private String employee_name;
    private int position_id;

    public Employee() {
    }

    public Employee(int employee_id, String employee_name, int positionId) {
        this.employee_id = employee_id;
        this.employee_name = employee_name;
        position_id = positionId;
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public int getPosition_id() {
        return position_id;
    }

    public void setPosition_id(int position_id) {
        this.position_id = position_id;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employee_id=" + employee_id +
                ", employee_name='" + employee_name + '\'' +
                ", position_id=" + position_id +
                '}';
    }
}