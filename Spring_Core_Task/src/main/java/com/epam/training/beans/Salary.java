package com.epam.training.beans;

public class Salary {

    private int salary;
    private int inflationCoefficient;

    public Salary() {
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getInflationCoefficient() {
        return inflationCoefficient;
    }

    public void setInflationCoefficient(int inflationCoefficient) {
        this.inflationCoefficient = inflationCoefficient;
    }

    @Override
    public String toString() {
        return " " + salary;
    }
}
