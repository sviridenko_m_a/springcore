package com.epam.training.services;

import com.epam.training.connections.ConnectionPool;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.*;

public class PositionServiceTest {

    PositionService positionService = new PositionService();

    @Test
    public void createPosition() {
        positionService.createPosition("newPosition", 987654);
        String result = getName(987654);
        assertEquals("newPosition", result);
    }

    @Test
    public void readPosition() {
    }

    @Test
    public void updatePosition() {
        positionService.updatePosition(10, "newPosition", 9876543);
        String result = getName(9876543);
        assertEquals("newPosition", result);
    }

    @Test
    public void deletePosition() {
        positionService.updatePosition(10, "newPosition", 9876542);
        positionService.deletePosition(10);
        assertNull(getName(9876542));

    }

    /**
     * Method return employee name by unique position
     */
    private String getName(int uniqPositionRate) {
        String POSITION_NAME = "SELECT position_name FROM position WHERE (position_rate = ?)";
        String positionName = null;
        try (Connection cn = ConnectionPool.getConnection();
             PreparedStatement ps = cn.prepareStatement(POSITION_NAME)) {
            ps.setInt(1, uniqPositionRate);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                positionName = rs.getString(1);
                ps.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return positionName;
    }
}