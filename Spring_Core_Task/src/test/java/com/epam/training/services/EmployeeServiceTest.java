package com.epam.training.services;

import com.epam.training.connections.ConnectionPool;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.*;

public class EmployeeServiceTest {

    private static Logger LOGGER = Logger.getLogger("LOGGER");
    EmployeeService employeeService = new EmployeeService();

    @Test
    public void hireEmployee() {
        employeeService.hireEmployee("newName", 447);
        String result = getName(447);
        assertEquals("newName", result);
    }

    @Test
    public void fireEmployee() {
        String result = getName(447);
        assertNotNull("newName", result);
        employeeService.fireEmployee(133);
        result = getName(447);
        assertNull("newName", result);
    }

    /**
     * Method return employee name by unique position
     */
    private String getName(int uniqPosition) {
        String EMPLOYEE_NAME = "SELECT employee_name FROM employee WHERE (position_id = ?)";
        String positionName = null;
        try (Connection cn = ConnectionPool.getConnection();
             PreparedStatement ps = cn.prepareStatement(EMPLOYEE_NAME)) {
            ps.setInt(1, uniqPosition);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                positionName = rs.getString(1);
                ps.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return positionName;
    }
}