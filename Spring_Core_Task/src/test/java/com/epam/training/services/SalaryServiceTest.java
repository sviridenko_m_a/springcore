package com.epam.training.services;

import org.junit.Test;

import static org.junit.Assert.*;

public class SalaryServiceTest {

    @Test
    public void calculateSalaryWithInflationCoefficient() {
        int one = 1;
        int two = 2;
        SalaryService salaryService = new SalaryService();
        int result = salaryService.calculateSalaryWithInflationCoefficient(one, two);
        assertEquals(3, result);
    }
}